#ifndef _STONE_MESH
#define _STONE_MESH

#include "ofMain.h"

class stoneMesh{
    public:
    stoneMesh();
    void createNewMesh();
    vector<ofMesh> meshes;
};

#endif
/*

#include "testApp.h"

struct Ball {
    ofPoint pos;
    int alpha;
};

vector<Ball> balls;

//--------------------------------------------------------------
void testApp::setup() {
    ofBackground(0,0,0);
    ofEnableAlphaBlending();
}

//--------------------------------------------------------------
void testApp::draw() {
    for (int i=0; i<balls.size(); i++) {
        balls[i].alpha--;
        ofSetColor(255,255,255,balls[i].alpha);
        ofCircle(balls[i].pos.x, balls[i].pos.y,20);
    }
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y) {
    Ball ball;
    ball.pos.x = x;
    ball.pos.y = y;
    ball.alpha = 255;
    balls.push_back(ball);

    if (balls.size()>50) balls.erase(balls.begin());
}
*/
