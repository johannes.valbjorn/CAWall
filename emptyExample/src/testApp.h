#pragma once

#include "ofMain.h"
#include "textSlide.h"
#include "loppenDiagonal.h"
#include "src/ofxBezierWarp.h"
#include "diagonal.h"
#include "ofxXmlSettings.h"
#include "ofxIpVideoGrabber.h"

class testApp : public ofBaseApp
{
public:
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void loadStreams();

    textSlide myTextSlide;
    loppenDiagonal ld;
    ofxBezierWarp diaWarp;
    diagonal dia;

    vector< string> URLS;
    vector<ofFbo> fbos;
    //vector<ofTexture> tex;
    vector< ofxSharedIpVideoGrabber > ipGrabber;
};
