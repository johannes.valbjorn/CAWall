#ifndef _DIAGONAL
#define _DIAGONAL

#include "ofMain.h"
#include "ofxBezierWarp.h"

class diagonal
{
protected:
public:
    int width;
    int height;
    diagonal();
    void setup(int w, int h, int p, vector<ofFbo> fbos2, string name);
    void draw();
    void save();
    void load();
    void keyPressed(int key);
    void mouseDragged(int x, int y);
    void mousePressed(int x, int y);
    void update();
    vector<ofFbo> fbos;
    ofFbo fbo;
    vector<ofVec2f> v2;
    ofxBezierWarp warp;

private:
    string name;
    void initv2();
    int indexAtZero;
    int indexAtMaxMinus;
    float startTime; // store when we start time timer
    float endTime; // when do want to stop the timer

    bool  bTimerReached; // used as a trigger when we hit
};
#endif

