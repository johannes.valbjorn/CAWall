#include "loppenDiagonal.h"

loppenDiagonal::loppenDiagonal()
{
}
void loppenDiagonal::setup(int a,int w, int h)
{
    //timer stuff
    bTimerReached = false;
    startTime = ofGetElapsedTimeMillis();  // get the start time
    endTime = ofGetElapsedTimeMillis()+2000.0; // in milliseconds

    width=w;
    height=h;

    fbo.allocate(width, height, GL_RGBA);
    fbo.begin();

    ofClear(0,0,0, 0);
    fbo.end();
    ofBackground(0,0,0);
    v2 = new ofVec2f[amountOfObjects];
    v2[0]=ofVec2f(0,0);
    indexAtZero=0;
    indexAtMaxMinus=0;

    for(int i=0; i<amountOfObjects; i++)
    {
        ts[i] = textSlide();
        ts[i].setup(height,"","", "", "");
        if(i>0)
        {
            v2[i] = ofVec2f(v2[i-1].x-ts[i].getWidth(),0);
            indexAtMaxMinus=i;
            int a = ofGetElapsedTimeMillis();
            stringstream ss;
            ss << a;
            string str = ss.str()+ss.str();
            addElement(str,"trol","trolol","rofl");
        }
    }

}

void loppenDiagonal::addElement(string b, string s, string p, string d)
{
    for(int i=amountOfObjects-1; i>0; i--)
    {
        ts[i]=ts[i-1];
    }
    ts[0].setup(height,b,s,p,d);
}
void loppenDiagonal::update()
{
    if(endTime<ofGetElapsedTimeMillis())
    {
        if(v2[indexAtZero].x<0)
        {
            fbo.begin();
            ofClear(0,0,0,0);
            for(int i=0; i<amountOfObjects; i++)
            {
                v2[i].x++;
                if(v2[i].x>width)
                {
                    v2[i].x=v2[indexAtMaxMinus].x-ts[i].getWidth();
                    indexAtMaxMinus=i;
                    }
                    ts[i].returnFbo().draw(v2[i]);
                }
            fbo.end();

        }
        else
        {
            //lav så de looper eternally
            indexAtZero+=1;

            if(indexAtZero>amountOfObjects-1)
            {
                indexAtZero=0;

            }
            endTime=ofGetElapsedTimeMillis()+1000;
        }
    }
}
ofTexture loppenDiagonal::getTexture()
{
    return fbo.getTextureReference();
}
