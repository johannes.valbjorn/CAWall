#include "texts.h"

texts::texts(){
    xdir=1;
    ydir=1;
}
void texts::setup(int width2, int height2){
    width=width2;
    height=height2;
    x=int(width/7);
    y=int(height/2);
}
void texts::update(){
    if(x+xdir>width-1 || x+xdir<1){
        xdir*=-1;
    }
        if(y+ydir>height-1 || y+ydir<1){
        ydir*=-1;
    }
    x+=xdir;
    y+=2*ydir;
}

void texts::draw(){
    ofFill();
    ofSetColor(255,255,0,255);
	ofRect(x,y,50,50);
	ofNoFill();
    ofSetColor(0,0,0,255);
}
