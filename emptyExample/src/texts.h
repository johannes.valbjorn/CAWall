#ifndef _OF_TEXTS
#define _OF_TEXTS

#include "ofMain.h"

class texts {
    public:
    //methods
    void setup(int width2, int height2);
    void update();
    void draw();
    //constructor
    texts();
    int height;
    int width;
    int x;
    int y;
    int xdir;
    int ydir;

    private:

};

#endif
