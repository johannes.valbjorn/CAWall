#include "diagonal.h";

diagonal::diagonal()
{
}
void diagonal::setup(int w, int h, int p, vector<ofFbo> fbos2, string name)
{
    fbos=fbos2;
    width=w;
    height=h;
    stringstream ss;
    ss << name << ".bin";
    warp.setup(p,ss.str());

    fbo.allocate(width, height, GL_RGBA);
    fbo.begin();
    ofClear(0,0,0, 255);
    fbo.end();

    //timer stuff
    bTimerReached = false;
    startTime = ofGetElapsedTimeMillis();  // get the start time
    endTime = ofGetElapsedTimeMillis()+2000.0; // in milliseconds

    indexAtZero=0;
    indexAtMaxMinus=0;

    for(int i=0; i<fbos.size(); i++)
    {
        if(i>0)
        {
            ofVec2f v=ofVec2f(v2[i-1].x-fbos[i].getWidth(),0);
            v2.push_back(v);
            indexAtMaxMinus=i;
        }
        else
        {
            ofVec2f v=ofVec2f(0,0);
            v2.push_back(v);

        }
    }

}
void diagonal::update()
{
    if(endTime<ofGetElapsedTimeMillis())
    {
        if(v2[indexAtZero].x<0)
        {
            fbo.begin();
            ofClear(0,0,0,0);
            for(int i=0; i<fbos.size(); i++)
            {
                v2[i].x++;
                if(v2[i].x>width)
                {
                    v2[i].x=v2[indexAtMaxMinus].x-fbos[i].getWidth();

                    indexAtMaxMinus=i;
                }
                fbos[i].draw(v2[i].x,0);
            }
            fbo.end();

        }
        else
        {
            //lav så de looper eternally
            indexAtZero+=1;

            if(indexAtZero>fbos.size()-1)
            {
                indexAtZero=0;

            }
            endTime=ofGetElapsedTimeMillis()+1000;
        }
    }
}

void diagonal::draw()
{
    //tex[0].draw(0,0);
    warp.draw(fbo.getTextureReference());

}

void diagonal::keyPressed(int key)
{
    warp.keyPressed(key);
}

void diagonal::mouseDragged(int x, int y)
{
    warp.mouseDragged(x, y);

}

void diagonal::mousePressed(int x, int y)
{
    warp.mousePressed(x,y);
}


void diagonal::save()
{
    warp.save();
}

void diagonal::load()
{
    warp.recall();
}
