#ifndef _OF_TEXTSLIDE
#define _OF_TEXTSLIDE

#include "ofMain.h"

class textSlide {
    public:
    void setup(int h, string b, string s, string p, string d);

    textSlide();
    void update();

    int width;
    int height;

    ofFbo fbo;
    ofTexture returnFbo();
        int getWidth();


    private:

    void leftZone();
    void rightTopZone();
    void rightMiddleZone();
    void rightBottomLeftZone();

    string band;
    string support;
    string price;
    string date;

    ofTrueTypeFont	fontA;
    ofTrueTypeFont	fontB;
    ofTrueTypeFont	fontC;

};
#endif
