#ifndef _LOPPEN_DIAGONAL
#define _LOPPEN_DIAGONAL

#include "ofMain.h"
#include "textSlide.h"
class loppenDiagonal{

    public:
    loppenDiagonal();
    void setup(int a,int w, int h);
    void update();
    ofTexture getTexture();
    void addElement(string b, string s, string p, string d);
    private:
    int width;
    int height;
ofTexture *tex;
textSlide *ts;
    ofFbo fbo;
    int amountOfObjects;
    ofVec2f *v2;

    int indexAtZero;
    int indexAtMaxMinus;
        float startTime; // store when we start time timer
		float endTime; // when do want to stop the timer

		bool  bTimerReached; // used as a trigger when we hit
};
#endif
