#include "textSlide.h"

textSlide::textSlide()
{
    band="copenhagen ska convention 2012";
    support = "megadeath";
    price="pris 1000kr";
    date = "15 okt";
}

void textSlide::setup(int h,string b, string s, std::string p, std::string d)
{
    //strings
    band=b;
    support=s;
    price=p;
    date=d;



    //fot stuff
    ofTrueTypeFont::setGlobalDpi(72);
    fontA.loadFont("verdana.ttf", 40, true, true, true);
	fontA.setLineHeight(40.0f);
	fontA.setLetterSpacing(1.037);

    fontB.loadFont("verdana.ttf", 30, true, true,true);
	fontB.setLineHeight(30.0f);
	fontB.setLetterSpacing(1.037);

    fontC.loadFont("verdana.ttf", 20, true, true,true);
	fontC.setLineHeight(20.0f);
	fontC.setLetterSpacing(1.037);

        //size stuff
    height=h;
    ofRectangle rect1 = fontA.getStringBoundingBox(band, 0,0);
    ofRectangle rect2 = fontB.getStringBoundingBox(support, 0,0);

    if(rect2.width>rect1.width){
        width=rect2.width;
    }else{
        width=rect1.width;
    }
   width=width+height+4;

	//fbo stuff
    fbo.allocate(width, height, GL_RGBA);
    fbo.begin();
    ofClear(0,0,0, 0);
    fbo.end();
    ofBackground(0,0,0);

    //draw
        fbo.begin();
    ofClear(0,0,0, 0);
    leftZone();
    rightTopZone();
    rightMiddleZone();
    rightBottomLeftZone();
    fbo.end();
}
void textSlide::update()
{

}

int textSlide::getWidth(){
    return width;
}

void textSlide::leftZone()
{
    ofSetColor(255,255,255,255);
    ofRect(0,0,height,height);
    ofSetColor(0,0,0,255);
        fontC.drawStringAsShapes(date, 0, 20);
            ofSetColor(255,255,255,255);


}

void textSlide::rightTopZone()
{
	fontA.drawStringAsShapes(band, height+4, height/6*3-12);
   // ofRect(height+4,0,width-height-4,height/2-2);
}

void textSlide::rightMiddleZone()
{
    fontB.drawStringAsShapes(support, height+4, height/6*4.5-2);
    //ofRect(height+4,height/2+2,width-height-4,height/4-4);
}

void textSlide::rightBottomLeftZone()
{
    fontC.drawStringAsShapes(price, height+4, height-2);
   // ofRect(height+4,(height-height/4)+2,(width-height-4)/2-2,height/4-2);
}

ofTexture textSlide::returnFbo()
{
    return fbo.getTextureReference();
}
