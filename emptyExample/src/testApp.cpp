#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup()
{
   // ld.setup(10,1000, 100);
    ofEnableAlphaBlending();
    ofEnableSmoothing();

    loadStreams();

   // diaWarp.setup(10,"diaWarp.bin");

for(int i=0;i<10;i++){
    ofxSharedIpVideoGrabber c( new ofxIpVideoGrabber());
    c->setCameraName("hej");
    c->setURI(URLS[i]);
    c->connect();
    ipGrabber.push_back(c);

    ofFbo fbo;
    fbo.allocate(320, 240, GL_RGBA);

    fbo.begin();
    ofClear(i*10,255-(i*10),0, 255);
    ipGrabber[0]->draw(0,0);
    fbo.end();
    fbos.push_back(fbo);
}
     dia.setup(1000,240,10,fbos,"hello");


for(int i=0;i<10;i++){
}

     //dia.save();
	ofLog(OF_LOG_NOTICE, "main setup done");

}
void testApp::loadStreams() {

    // all of these cameras were found using this google query
    // http://www.google.com/search?q=inurl%3A%22axis-cgi%2Fmjpg%22
    // some of the cameras below may no longer be valid.

    // to define a camera with a username / password
    //ipcams.push_back(IPCameraDef("http://148.61.142.228/axis-cgi/mjpg/video.cgi", "username", "password"));

	ofLog(OF_LOG_NOTICE, "---------------Loading Streams---------------");

	ofxXmlSettings XML;

	if( XML.loadFile("streams.xml") ){

        XML.pushTag("streams");
		string tag = "stream";

		int nCams = XML.getNumTags(tag);

		for(int n = 0; n < nCams; n++) {
URLS.push_back(XML.getAttribute(tag, "url", "", n));
           /* IPCameraDef def;

			def.name = XML.getAttribute(tag, "name", "", n);
			def.url = XML.getAttribute(tag, "url", "", n);
			def.username = XML.getAttribute(tag, "username", "", n);
			def.password = XML.getAttribute(tag, "password", "", n);

			string logMessage = "STREAM LOADED: " + def.name +
			" url: " +  def.url +
			" username: " + def.username +
			" password: " + def.password;

            ofLog(OF_LOG_NOTICE, logMessage);

            ipcams.push_back(def);
*/
		}

		XML.popTag();



	} else {
		ofLog(OF_LOG_ERROR, "Unable to load streams.xml.");
	}
	ofLog(OF_LOG_NOTICE, "-----------Loading Streams Complete----------");


}

//--------------------------------------------------------------
void testApp::update()
{
    dia.update();
        // update the cameras
    for(size_t i = 0; i < ipGrabber.size(); i++)
    {
        ipGrabber[i]->update();

    fbos[i].begin();
//    ofClear(i*10,255-(i*10),0, 255);
    ipGrabber[i]->draw(0,0);
    fbos[i].end();
    }
    //ld.update();
}

//--------------------------------------------------------------
void testApp::draw()
{
    fbos[0].draw(0,0);
    dia.draw();

//myTextSlide.returnFbo().draw(0,0);
//ld.getTexture().draw(0,0);
    //diaWarp.draw(ld.getTexture());
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
    //diaWarp.keyPressed(key);
    dia.keyPressed(key);

}

//--------------------------------------------------------------
void testApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y)
{

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{
    //diaWarp.mouseDragged(x, y);
    dia.mouseDragged(x,y);

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{
    //diaWarp.mousePressed(x, y);
    dia.mousePressed(x,y);
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg)
{

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo)
{

}
